<?php

namespace Tests\Skeleton\UI\RestBundle\Controller\Home;

use Lakion\ApiTestCase\JsonApiTestCase;

class PizzaControllerTest extends JsonApiTestCase
{
    public function setUp()
    {
        $this->setUpClient();
        $this->expectedResponsesPath = $this->client->getContainer()->getParameter('kernel.root_dir') . "/../tests/UI/Responses/Home";
    }

    /**
     * @group functional
     */
    public function testGetAction()
    {
        $this->client->request('GET', '/');

        self::assertResponse($this->client->getResponse(), "home");
    }
}
