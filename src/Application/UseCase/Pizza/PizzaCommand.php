<?php
namespace Skeleton\Application\UseCase\Pizza;


use Skeleton\Application\UseCase\Pizza\Request\CreatePizzaRequest;
use Skeleton\Application\UseCase\Pizza\Request\UpdatePizzaRequest;
use Skeleton\Domain\Pizza\Factory\PizzaFactoryInterface;
use Skeleton\Domain\Pizza\Repository\PizzaRepositoryInterface;

/**
 * Class PizzaCommand
 * @package Skeleton\Application\UseCase\Pizza
 */
class PizzaCommand
{
    /**
     * @var PizzaRepositoryInterface
     */
    private $pizzaRepository;

    /**
     * @var PizzaFactoryInterface
     */
    private $pizzaFactory;

    /**
     * PizzaCommand constructor.
     * @param PizzaRepositoryInterface $pizzaRepository
     * @param PizzaFactoryInterface $pizzaFactory
     */
    public function __construct(PizzaRepositoryInterface $pizzaRepository, PizzaFactoryInterface $pizzaFactory)
    {
        $this->pizzaRepository = $pizzaRepository;
        $this->pizzaFactory = $pizzaFactory;
    }

    /**
     *
     * @param CreatePizzaRequest $pizzaRequest
     * @return \Skeleton\Domain\Pizza\Model\Pizza
     */
    public function create(CreatePizzaRequest $pizzaRequest)
    {
        $pizza = $this->pizzaFactory->create($pizzaRequest->toForm());
        $this->pizzaRepository->store($pizza);

        return $pizza;
    }

    /**
     *
     * @param UpdatePizzaRequest $pizzaRequest
     * @return \Skeleton\Domain\Pizza\Model\Pizza
     */
    public function update(UpdatePizzaRequest $pizzaRequest)
    {
        return $this->pizzaFactory->update($pizzaRequest->getPizza(), $pizzaRequest->toForm());
    }
}
