<?php
namespace Skeleton\Application\UseCase\Pizza;

use Skeleton\Application\UseCase\PaginationRequest;
use Skeleton\Domain\Pizza\Model\Pizza;
use Skeleton\Domain\Pizza\Repository\PizzaRepositoryInterface;

/**
 * Class PizzaQuery
 *
 * @package Skeleton\Application\UseCase\Pizza
 */
class PizzaQuery
{
    private $pizzaRepository;

    public function __construct(PizzaRepositoryInterface $pizzaRepository)
    {
        $this->pizzaRepository = $pizzaRepository;
    }

    public function findById(int $id): Pizza
    {
        return $this->pizzaRepository->findById($id);
    }

    /**
     * @param PaginationRequest $request
     *
     * @return mixed
     */
    public function all(PaginationRequest $request): array
    {
        return $this->pizzaRepository->findAll(
            $request->getFilters(),
            $request->getOperators(),
            $request->getValues(),
            $request->getSort()
        );
    }
}