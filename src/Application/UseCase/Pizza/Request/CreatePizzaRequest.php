<?php
namespace Skeleton\Application\UseCase\Pizza\Request;

/**
 * Class CreatePizzaRequest
 * @package Skeleton\Application\UseCase\Pizza\Request
 */
class CreatePizzaRequest
{
    /**
     * @var string
     */
    private $name;

    /**
     * @var float
     */
    private $sellingPrice;

    /**
     * @var array
     */
    private $ingredients;

    /**
     * CreatePizzaRequest constructor.
     *
     * @param string $name
     * @param float $sellingPrice
     * @param array $ingredients
     */
    public function __construct(string $name, float $sellingPrice, array $ingredients)
    {
        $this->name = $name;
        $this->sellingPrice = $sellingPrice;
        $this->ingredients = $ingredients;
    }

    /**
     *
     * @return array
     */
    public function toForm(): array
    {
        return [
            'name'          => $this->name,
            'sellingPrice'  => $this->sellingPrice,
            'ingredients'   => $this->ingredients
        ];
    }
}
