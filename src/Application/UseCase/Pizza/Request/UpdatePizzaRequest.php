<?php
namespace Skeleton\Application\UseCase\Pizza\Request;

use Skeleton\Domain\Pizza\Model\Pizza;

/**
 * Class UpdatePizzaRequest
 *
 * @package Skeleton\Application\UseCase\Pizza\Request
 */
class UpdatePizzaRequest
{
    /**
     * @var Pizza
     */
    private $pizza;

    /**
     * @var array
     */
    private $formTree;

    /**
     * UpdatePizzaRequest constructor.
     *
     * @param Pizza $pizza
     * @param array $formTree
     */
    public function __construct(Pizza $pizza, array $formTree)
    {
        $this->pizza = $pizza;
        $this->formTree = $formTree;
    }

    /**
     *
     * @return Pizza
     */
    public function getPizza(): Pizza
    {
        return $this->pizza;
    }

    /**
     *
     * @return array
     */
    public function toForm(): array
    {
        return $this->formTree;
    }
}
