<?php
namespace Skeleton\Application\UseCase\Ingredient\Request;

use Skeleton\Domain\Ingredient\Model\Ingredient;

/**
 * Class UpdateIngredientRequest
 *
 * @package Skeleton\Application\UseCase\Ingredient\Request
 */
class UpdateIngredientRequest
{
    /**
     * @var Ingredient
     */
    private $ingredient;

    /**
     * @var array
     */
    private $formTree;

    /**
     * UpdateIngredientRequest constructor.
     *
     * @param Ingredient $ingredient
     * @param array $formTree
     */
    public function __construct(Ingredient $ingredient, array $formTree)
    {
        $this->ingredient = $ingredient;
        $this->formTree = $formTree;
    }

    /**
     *
     * @return Ingredient
     */
    public function getIngredient(): Ingredient
    {
        return $this->ingredient;
    }

    /**
     *
     * @return array
     */
    public function toForm(): array
    {
        return $this->formTree;
    }
}
