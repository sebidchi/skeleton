<?php
namespace Skeleton\Application\UseCase\Ingredient\Request;

/**
 * Class CreateIngredientRequest
 * @package Skeleton\Application\UseCase\Ingredient\Request
 */
class CreateIngredientRequest
{
    /**
     * @var string
     */
    private $name;

    /**
     * @var float
     */
    private $costPrice;

    /**
     * CreateIngredientRequest constructor.
     *
     * @param string $name
     * @param float $costPrice
     */
    public function __construct(string $name, float $costPrice)
    {
        $this->name = $name;
        $this->costPrice = $costPrice;
    }

    /**
     *
     * @return array
     */
    public function toForm(): array
    {
        return [
            'name'      => $this->name,
            'costPrice' => $this->costPrice
        ];
    }
}
