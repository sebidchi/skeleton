<?php
namespace Skeleton\Application\UseCase\Ingredient;

use Skeleton\Application\UseCase\PaginationRequest;
use Skeleton\Domain\Ingredient\Model\Ingredient;
use Skeleton\Domain\Ingredient\Repository\IngredientRepositoryInterface;

/**
 * Class IngredientQuery
 *
 * @package Skeleton\Application\UseCase\Ingredient
 */
class IngredientQuery
{
    /**
     * @var IngredientRepositoryInterface
     */
    private $ingredientRepository;

    /**
     * IngredientQuery constructor.
     *
     * @param IngredientRepositoryInterface $ingredientRepository
     */
    public function __construct(IngredientRepositoryInterface $ingredientRepository)
    {
        $this->ingredientRepository = $ingredientRepository;
    }

    /**
     *
     * @param int $id
     *
     * @return Ingredient
     */
    public function findById(int $id): Ingredient
    {
        return $this->ingredientRepository->findById($id);
    }

    /**
     *
     * @param PaginationRequest $request
     *
     * @return array
     */
    public function all(PaginationRequest $request): array
    {
        return $this->ingredientRepository->findAll(
            $request->getFilters(),
            $request->getOperators(),
            $request->getValues(),
            $request->getSort()
        );
    }
}
