<?php
namespace Skeleton\Application\UseCase\Ingredient;

use Skeleton\Application\UseCase\Ingredient\Request\CreateIngredientRequest;
use Skeleton\Application\UseCase\Ingredient\Request\UpdateIngredientRequest;
use Skeleton\Domain\Ingredient\Factory\IngredientFactoryInterface;
use Skeleton\Domain\Ingredient\Repository\IngredientRepositoryInterface;

/**
 * Class IngredientCommand
 *
 * @package Skeleton\Application\UseCase\Ingredient
 */
class IngredientCommand
{
    /**
     * @var IngredientRepositoryInterface
     */
    private $ingredientRepository;

    /**
     * @var IngredientFactoryInterface
     */
    private $ingredientFactory;

    /**
     * IngredientCommand constructor.
     *
     * @param IngredientRepositoryInterface $ingredientRepository
     * @param IngredientFactoryInterface $ingredientFactory
     */
    public function __construct(IngredientRepositoryInterface $ingredientRepository, IngredientFactoryInterface $ingredientFactory)
    {
        $this->ingredientRepository = $ingredientRepository;
        $this->ingredientFactory = $ingredientFactory;
    }

    /**
     *
     * @param CreateIngredientRequest $ingredientRequest
     *
     * @return \Skeleton\Domain\Ingredient\Model\Ingredient
     */
    public function create(CreateIngredientRequest $ingredientRequest)
    {
        $ingredient = $this->ingredientFactory->create($ingredientRequest->toForm());
        $this->ingredientRepository->store($ingredient);

        return $ingredient;
    }

    /**
     *
     * @param UpdateIngredientRequest $ingredientRequest
     *
     * @return \Skeleton\Domain\Ingredient\Model\Ingredient
     */
    public function update(UpdateIngredientRequest $ingredientRequest)
    {
        return $this->ingredientFactory->update($ingredientRequest->getIngredient(), $ingredientRequest->toForm());
    }
}
