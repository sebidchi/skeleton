<?php
namespace Skeleton\Application\UseCase;

use Skeleton\Application\Request\Common\PaginationDTO;

/**
 * Class PaginationRequest
 *
 * @package Skeleton\Application\UseCase
 */
class PaginationRequest extends PaginationDTO
{

}