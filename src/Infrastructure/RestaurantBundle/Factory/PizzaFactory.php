<?php
namespace Skeleton\Infrastructure\RestaurantBundle\Factory;

use Skeleton\Domain\Pizza\Factory\PizzaFactoryInterface;
use Skeleton\Domain\Pizza\Model\Pizza;
use Skeleton\Infrastructure\RestaurantBundle\Form\PizzaType;
use Symfony\Component\Form\FormFactory;

/**
 * Class PizzaFactory
 * @package Skeleton\Infrastructure\RestaurantBundle\Factory
 */
class PizzaFactory extends AbstractFactory implements PizzaFactoryInterface
{
    /**
     * PizzaFactory constructor.
     *
     * @param FormFactory $formFactory
     */
    public function __construct(FormFactory $formFactory)
    {
        $this->formClass = PizzaType::class;
        parent::__construct($formFactory);
    }

    /**
     *
     * @param array $data
     * @return Pizza
     */
    public function create(array $data): Pizza
    {
        return $this->execute(self::CREATE, $data);
    }

    /**
     *
     * @param Pizza $pizza
     * @param array $data
     * @return Pizza
     */
    public function update(Pizza $pizza, array $data): Pizza
    {
        return $this->execute(self::UPDATE, $data);
    }

    /**
     *
     * @param Pizza $pizza
     * @param array $data
     * @return Pizza
     */
    public function replace(Pizza $pizza, array $data): Pizza
    {
        return $this->execute(self::REPLACE, $data);
    }
}
