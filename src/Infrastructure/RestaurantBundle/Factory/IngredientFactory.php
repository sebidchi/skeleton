<?php
namespace Skeleton\Infrastructure\RestaurantBundle\Factory;

use Skeleton\Domain\Ingredient\Factory\IngredientFactoryInterface;
use Skeleton\Domain\Ingredient\Model\Ingredient;
use Skeleton\Infrastructure\RestaurantBundle\Form\IngredientType;
use Symfony\Component\Form\FormFactory;

/**
 * Class IngredientFactory
 * @package Skeleton\Infrastructure\RestaurantBundle\Factory
 */
class IngredientFactory extends AbstractFactory implements IngredientFactoryInterface
{
    /**
     * IngredientFactory constructor.
     *
     * @param FormFactory $formFactory
     */
    public function __construct(FormFactory $formFactory)
    {
        $this->formClass = IngredientType::class;
        parent::__construct($formFactory);
    }

    /**
     *
     * @param array $data
     *
     * @return Ingredient
     */
    public function create(array $data): Ingredient
    {
        return $this->execute(self::CREATE, $data);
    }

    /**
     *
     * @param Ingredient $ingredient
     * @param array $data
     *
     * @return Ingredient
     */
    public function update(Ingredient $ingredient, array $data): Ingredient
    {
        return $this->execute(self::UPDATE, $data);
    }

    /**
     *
     * @param Ingredient $ingredient
     * @param array $data
     *
     * @return Ingredient
     */
    public function replace(Ingredient $ingredient, array $data): Ingredient
    {
        return $this->execute(self::REPLACE, $data);
    }
}
