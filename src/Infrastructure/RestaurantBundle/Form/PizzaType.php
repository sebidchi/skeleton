<?php
namespace Skeleton\Infrastructure\RestaurantBundle\Form;

use Doctrine\DBAL\Types\TextType;
use Skeleton\Domain\Ingredient\Model\Ingredient;
use Skeleton\Domain\Pizza\Model\Pizza;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\NotNull;


/**
 * Class PizzaType
 *
 * @package Skeleton\Infrastructure\RestaurantBundle\Form
 */
class PizzaType extends AbstractType
{
    /**
     *
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, [
                'constraints' => [
                    new NotBlank(
                        ['message'   => 'skeleton.pizza.name.not_blank']
                    ),
                    new NotNull(
                        ['message'  => 'skeleton.pizza.name.not_null']
                    )
                ]
            ])
            ->add('ingredients', EntityType::class, [
                'class'         => Ingredient::class,
                'choice_label'  => 'name',
                'multiple'      => true
            ])
        ;
    }

    /**
     *
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Pizza::class,
            'csrf_protection'   => false,
            'empty_data' => function(FormInterface $form){

                return new Pizza(
                    $form->get('name')->getData()
                );
            }
        ));
    }
}
