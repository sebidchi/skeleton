<?php
namespace Skeleton\Infrastructure\RestaurantBundle\Form;

use Doctrine\DBAL\Types\TextType;
use Skeleton\Domain\Ingredient\Model\Ingredient;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\NotNull;

/**
 * Class IngredientType
 * @package Skeleton\Infrastructure\RestaurantBundle\Form
 */
class IngredientType extends AbstractType
{
    /**
     *
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, [
                'constraints' => [
                    new NotBlank(
                        ['message'   => 'skeleton.ingredient.name.not_blank']
                    ),
                    new NotNull(
                        ['message'  => 'skeleton.ingredient.name.not_null']
                    )
                ]
            ])
            ->add('costPrice', NumberType::class)
        ;
    }

    /**
     *
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Ingredient::class,
            'csrf_protection'   => false,
            'empty_data' => function(FormInterface $form){

                return new Ingredient(
                    $form->get('name')->getData(),
                    $form->get('costPrice')->getData()
                );
            }
        ));
    }
}
