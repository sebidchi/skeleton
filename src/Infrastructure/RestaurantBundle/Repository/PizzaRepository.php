<?php
namespace Skeleton\Infrastructure\RestaurantBundle\Repository;

use Skeleton\Domain\Pizza\Model\Pizza;
use Skeleton\Domain\Pizza\Repository\PizzaRepositoryInterface;
use Skeleton\Infrastructure\UtilsBundle\Repository\EntityRepository;
use Pagerfanta\Pagerfanta;

/**
 * Class PizzaRepository
 *
 * @package Skeleton\Infrastructure\RestaurantBundle\Repository
 */
class PizzaRepository extends EntityRepository implements PizzaRepositoryInterface
{
    /**
     *
     * @param int $id
     *
     * @return Pizza
     */
    public function findById(int $id): Pizza
    {
        return $this->createQueryBuilder('pizza')
            ->where('pizza.id = :id')
            ->setParameter('id', $id)
            ->getQuery()
            ->getOneOrNullResult()
            ;
    }

    /**
     *
     * @param array $filters
     * @param array $operators
     * @param array $values
     * @param array $sort
     * @return mixed
     */
    public function findAll(array $filters = [], array $operators = [], array $values = [], array $sort = []): array
    {
        return $this->pagination($filters, $operators, $values, $sort);
    }

    /**
     *
     * @param Pizza $pizza
     *
     * @return Pizza
     */
    public function store(Pizza $pizza): Pizza
    {
        $this->_em->persist($pizza);
        $this->_em->flush($pizza);
    }

    /**
     *
     * @param array $filters
     * @param array $operators
     * @param array $values
     * @param array $sort
     *
     * @return Pagerfanta
     */
    private function pagination(array $filters = [], array $operators = [], array $values = [], array $sort = [])
    {
        $queryBuilder = $this->createQueryBuilder('pizza');

        return $this->createOperatorPaginator($queryBuilder, 'pizza', $filters, $operators, $values, $sort);
    }
}
