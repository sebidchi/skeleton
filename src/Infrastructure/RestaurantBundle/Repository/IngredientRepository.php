<?php
namespace Skeleton\Infrastructure\RestaurantBundle\Repository;

use Skeleton\Domain\Ingredient\Repository\IngredientRepositoryInterface;
use Skeleton\Domain\Ingredient\Model\Ingredient;
use Skeleton\Infrastructure\UtilsBundle\Repository\EntityRepository;
use Pagerfanta\Pagerfanta;

/**
 * Class IngredientRepository
 *
 * @package Skeleton\Infrastructure\RestaurantBundle\Repository
 */
class IngredientRepository extends EntityRepository implements IngredientRepositoryInterface
{
    /**
     *
     * @param int $id
     *
     * @return Ingredient
     */
    public function findById(int $id): Ingredient
    {
        return $this->createQueryBuilder('ingredient')
            ->where('ingredient.id = :id')
            ->setParameter('id', $id)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }

    /**
     *
     * @param array $filters
     * @param array $operators
     * @param array $values
     * @param array $sort
     * @return mixed
     */
    public function findAll(array $filters = [], array $operators = [], array $values = [], array $sort = []): array
    {
        return $this->pagination($filters, $operators, $values, $sort);
    }

    /**
     *
     * @param Ingredient $ingredient
     *
     * @return Ingredient
     */
    public function store(Ingredient $ingredient): Ingredient
    {
        $this->_em->persist($ingredient);
        $this->_em->flush($ingredient);
    }

    /**
     *
     * @param array $filters
     * @param array $operators
     * @param array $values
     * @param array $sort
     *
     * @return Pagerfanta
     */
    private function pagination(array $filters = [], array $operators = [], array $values = [], array $sort = [])
    {
        $queryBuilder = $this->createQueryBuilder('ingredient');

        return $this->createOperatorPaginator($queryBuilder, 'ingredient', $filters, $operators, $values, $sort);
    }
}
