<?php
namespace Skeleton\Infrastructure\UtilsBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Class SkeletonInfrastructureUtilsBundle
 * @package Wondy\Bundle\UtilsBundle
 */
class SkeletonInfrastructureUtilsBundle extends Bundle
{

}
