<?php
namespace Skeleton\Infrastructure\UtilsBundle\Exception;

use Symfony\Component\Form\Form;

/**
 * Class FormException
 *
 * Has to be dispatched on form errors inside factories
 * and return Form on controller to render all errors
 *
 * @package Wondy\Bundle\UtilsBundle\Exception\Form
 */
class FormException extends \Exception
{
    /**
     * @var Form
     */
    private $form;

    /**
     * FormException constructor.
     * @param Form $form
     * @param string $message
     * @param int $code
     * @param \Exception|null $prev
     */
    public function __construct(Form $form, string $message = 'Form Error', int $code = 8000, \Exception $prev = null)
    {
        parent::__construct($message, $code, $prev);

        $this->form = $form;
    }

    /**
     * @return Form
     */
    public function getForm(): Form
    {
        return $this->form;
    }
}
