<?php
namespace Skeleton\Domain\Pizza\Factory;

use Skeleton\Domain\Pizza\Model\Pizza;

/**
 * Interface PizzaFactoryInterface
 *
 * @package Skeleton\Domain\Pizza\Factory
 */
interface PizzaFactoryInterface
{

    /**
     * @param array $data
     *
     * @return Pizza
     */
    public function create(array $data): Pizza;

    /**
     * @param Pizza $pizza
     * @param array $data
     *
     * @return Pizza
     */
    public function replace(Pizza $pizza, array $data): Pizza;

    /**
     * @param Pizza $pizza
     * @param array $data
     *
     * @return Pizza
     */
    public function update(Pizza $pizza, array $data): Pizza;
}
