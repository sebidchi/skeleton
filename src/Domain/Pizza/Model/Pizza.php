<?php
namespace Skeleton\Domain\Pizza\Model;
use Skeleton\Domain\Ingredient\Exception\IngredientNotFoundException;
use Skeleton\Domain\Ingredient\Model\Ingredient;

/**
 * Class Pizza
 * @package Skeleton\Domain\Pizza\Model
 */
class Pizza
{
    /**
     * @var string
     */
    private $name;

    /**
     * @var
     */
    private $sellingPrice;

    /**
     * @var
     */
    private $ingredients;

    /**
     * Pizza constructor.
     *
     * @param string $name
     */
    public function __construct(string $name)
    {
        $this->name = $name;
    }

    /**
     * @return Ingredient[]
     */
    public function getIngredients()
    {
        return $this->ingredients;
    }

    /**
     * @param Ingredient[] $ingredients
     */
    public function setIngredientCollection($ingredients)
    {
        $this->ingredients = $ingredients;
    }

    /**
     * Calculates sellingPrice
     */
    private function calculate()
    {
        $this->sellingPrice = 0;

        /** @var Ingredient $ingredient */
        foreach ($this->ingredients as $ingredient)
        {
            $this->sellingPrice += $ingredient->getCostPrice();
        }
    }

    /**
     *
     * @param Ingredient $ingredient
     *
     * @return $this
     */
    public function addItem(Ingredient $ingredient)
    {
        $this->ingredients[] = $ingredient;
        $this->calculate();

        return $this;
    }

    /**
     *
     * @param Ingredient $ingredient
     *
     * @return $this
     * @throws IngredientNotFoundException
     */
    public function removeItem(Ingredient $ingredient)
    {
        $index = array_search($ingredient, $this->ingredients);

        if ($index){
            unset($this->ingredients[$index]);
        } else {
            throw new IngredientNotFoundException();
        }

        $this->calculate();

        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getSellingPrice()
    {
        return $this->sellingPrice;
    }

    /**
     *
     * @param $sellingPrice
     *
     * @return $this
     */
    public function setSellingPrice($sellingPrice)
    {
        $this->sellingPrice = $sellingPrice;

        return $this;
    }
}