<?php
namespace Skeleton\Domain\Pizza\Repository;

use Skeleton\Domain\Pizza\Model\Pizza;

/**
 * Interface PizzaRepositoryInterface
 *
 * @package Skeleton\Domain\Pizza\Repository
 */
interface PizzaRepositoryInterface
{
    /**
     *
     * @param int $id
     *
     * @return Pizza
     */
    public function findById(int $id): Pizza;

    /**
     *
     * @param array $filters
     * @param array $operators
     * @param array $values
     * @param array $sort
     *
     * @return array
     */
    public function findAll(array $filters = [], array $operators = [], array $values = [], array $sort = []): array;

    /**
     *
     * @param Pizza $pizza
     *
     * @return Pizza
     */
    public function store(Pizza $pizza): Pizza;
}
