<?php
namespace Skeleton\Domain\Pizza\Exception;

/**
 * Class PizzaNotFoundException
 * @package Skeleton\Domain\Pizza\Exception
 */
class PizzaNotFoundException extends \Exception
{
    /**
     * PizzaNotFoundException constructor.
     */
    public function __construct()
    {
        parent::__construct('pizza.exception.not_found');
    }
}
