<?php
namespace Skeleton\Domain\Ingredient\Exception;


/**
 * Class IngredientNotFoundException
 *
 * @package Skeleton\Domain\Ingredient\Exception
 */
class IngredientNotFoundException extends \Exception
{
    /**
     * IngredientNotFoundException constructor.
     */
    public function __construct()
    {
        parent::__construct('ingredient.exception.not_found');
    }
}
