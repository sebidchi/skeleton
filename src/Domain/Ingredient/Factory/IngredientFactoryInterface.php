<?php
namespace Skeleton\Domain\Ingredient\Factory;

use Skeleton\Domain\Ingredient\Model\Ingredient;

/**
 * Interface IngredientFactoryInterface
 *
 * @package Skeleton\Domain\Ingredient\Factory
 */
interface IngredientFactoryInterface
{
    /**
     * @param array $data
     *
     * @return Ingredient
     */
    public function create(array $data): Ingredient;

    /**
     * @param Ingredient $ingredient
     * @param array $data
     *
     * @return Ingredient
     */
    public function replace(Ingredient $ingredient, array $data): Ingredient;

    /**
     * @param Ingredient $ingredient
     * @param array $data
     *
     * @return Ingredient
     */
    public function update(Ingredient $ingredient, array $data): Ingredient;
}