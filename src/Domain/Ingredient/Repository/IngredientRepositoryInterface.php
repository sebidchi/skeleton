<?php
namespace Skeleton\Domain\Ingredient\Repository;

use Skeleton\Domain\Ingredient\Model\Ingredient;

/**
 * Interface IngredientRepositoryInterface
 *
 * @package Skeleton\Domain\Ingredient\Repository
 */
interface IngredientRepositoryInterface
{
    /**
     *
     * @param int $id
     *
     * @return Ingredient
     */
    public function findById(int $id): Ingredient;

    /**
     *
     * @param array $filters
     * @param array $operators
     * @param array $values
     * @param array $sort
     *
     * @return array
     */
    public function findAll(array $filters = [], array $operators = [], array $values = [], array $sort = []): array;

    /**
     *
     * @param Ingredient $ingredient
     *
     * @return Ingredient
     */
    public function store(Ingredient $ingredient): Ingredient;
}
