<?php
namespace Skeleton\Domain\Ingredient\Model;


/**
 * Class Ingredient
 * @package Skeleton\Domain\Ingredient\Model
 */
class Ingredient
{
    /**
     * @var string
     */
    private $name;

    /**
     * @var float
     */
    private $costPrice;

    /**
     * Ingredient constructor.
     *
     * @param string $name
     * @param float $costPrice
     */
    public function __construct(string $name, float $costPrice)
    {
        $this->name = $name;
        $this->costPrice = $costPrice;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return float
     */
    public function getCostPrice(): float
    {
        return $this->costPrice;
    }
}
