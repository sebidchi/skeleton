skeleton-restaurant
============

### Development

This project relies on Docker containers to bootstrap the development environment. So, please, 
install docker and Docker-compose to run it.

The infrastucture is based on: 

    - Php7
    - nginx
    - Mysql

In order to get started, make a local copy the Docker Compose template and spin up the containers.


```bash
$ cp etc/infrastructure/dev/docker-compose.yml docker-compose.yml
$ docker-compose up -d
```

To exec the fpm execute and install vendors:

```bash
$ docker exec -it skeletonddd_app_1 sh -l
$ composer install 
```

To run the test

```
$ docker exec -it skeletonddd_app_1 phpunit
```

Any small tweaks to the development environment must go to your local copy, ```docker-compose.yml```.
This file is excluded from the Git repository.